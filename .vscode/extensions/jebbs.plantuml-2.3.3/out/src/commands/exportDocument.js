"use strict";
const common_1 = require('./common');
const exportDocument_1 = require('../plantuml/exporter/exportDocument');
class CommandExportDocument extends common_1.Command {
    constructor() {
        super("plantuml.exportDocument");
    }
    execute() {
        exportDocument_1.exportDocument(true);
    }
}
exports.CommandExportDocument = CommandExportDocument;
//# sourceMappingURL=exportDocument.js.map