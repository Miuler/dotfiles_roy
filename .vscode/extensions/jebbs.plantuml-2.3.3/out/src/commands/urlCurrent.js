"use strict";
const common_1 = require('./common');
const urlDocument_1 = require('../plantuml/urlMaker/urlDocument');
class CommandURLCurrent extends common_1.Command {
    constructor() {
        super("plantuml.URLCurrent");
    }
    execute() {
        urlDocument_1.makeDocumentURL(false);
    }
}
exports.CommandURLCurrent = CommandURLCurrent;
//# sourceMappingURL=urlCurrent.js.map