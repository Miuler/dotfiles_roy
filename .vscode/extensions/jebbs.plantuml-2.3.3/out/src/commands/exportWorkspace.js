"use strict";
const common_1 = require('./common');
const exportWorkSpace_1 = require('../plantuml/exporter/exportWorkSpace');
class CommandExportWorkspace extends common_1.Command {
    constructor() {
        super("plantuml.exportWorkspace");
    }
    execute(uri) {
        exportWorkSpace_1.exportWorkSpace(uri);
    }
}
exports.CommandExportWorkspace = CommandExportWorkspace;
//# sourceMappingURL=exportWorkspace.js.map