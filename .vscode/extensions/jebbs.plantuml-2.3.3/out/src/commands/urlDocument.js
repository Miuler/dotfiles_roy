"use strict";
const common_1 = require('./common');
const urlDocument_1 = require('../plantuml/urlMaker/urlDocument');
class CommandURLDocument extends common_1.Command {
    constructor() {
        super("plantuml.URLDocument");
    }
    execute() {
        urlDocument_1.makeDocumentURL(true);
    }
}
exports.CommandURLDocument = CommandURLDocument;
//# sourceMappingURL=urlDocument.js.map