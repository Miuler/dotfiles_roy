"use strict";
const common_1 = require('./common');
const exportDocument_1 = require('../plantuml/exporter/exportDocument');
class CommandExportCurrent extends common_1.Command {
    constructor() {
        super("plantuml.exportCurrent");
    }
    execute() {
        exportDocument_1.exportDocument(false);
    }
}
exports.CommandExportCurrent = CommandExportCurrent;
//# sourceMappingURL=exportCurrent.js.map