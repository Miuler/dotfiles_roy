"use strict";
const vscode = require('vscode');
const common_1 = require('../plantuml/common');
const tools_1 = require('../plantuml/tools');
const formatRules_1 = require('../plantuml/formatRules');
const fmt = require('../plantuml/formatter/formatter');
class Formatter {
    constructor() {
        this._formatter = new fmt.Formatter(formatRules_1.formatRules, {
            allowInlineFormat: false,
            allowSplitLine: true,
            newLineForBlockStart: false
        });
    }
    provideDocumentFormattingEdits(document, options, token) {
        try {
            return this._formatter.formate(document, options, token);
        }
        catch (error) {
            tools_1.showMessagePanel(common_1.outputPanel, tools_1.parseError(error));
        }
    }
    register() {
        let ds = [];
        let d = vscode.languages.registerDocumentFormattingEditProvider({ language: "diagram" }, this);
        ds.push(d);
        return ds;
    }
}
exports.formatter = new Formatter();
//# sourceMappingURL=formatter.js.map