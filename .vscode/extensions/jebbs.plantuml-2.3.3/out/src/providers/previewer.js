"use strict";
const vscode = require('vscode');
const fs = require('fs');
const path = require('path');
const diagram_1 = require('../plantuml/diagram/diagram');
const config_1 = require('../plantuml/config');
const common_1 = require('../plantuml/common');
const tools_1 = require('../plantuml/tools');
const exportToBuffer_1 = require("../plantuml/exporter/exportToBuffer");
var previewStatus;
(function (previewStatus) {
    previewStatus[previewStatus["default"] = 0] = "default";
    previewStatus[previewStatus["error"] = 1] = "error";
    previewStatus[previewStatus["processing"] = 2] = "processing";
})(previewStatus || (previewStatus = {}));
class Previewer {
    constructor() {
        this.Emittor = new vscode.EventEmitter();
        this.onDidChange = this.Emittor.event;
        this.Uri = vscode.Uri.parse('plantuml://preview');
        this.watchDisposables = [];
        this.error = "";
        this.zoomUpperLimit = true;
        this.killingLock = false;
    }
    initialize() {
        this.reset();
    }
    reset() {
        let tplPath = path.join(common_1.context.extensionPath, "templates");
        let tplPreviewPath = path.join(tplPath, "preview.html");
        let tplPreviewProcessingPath = path.join(tplPath, "preview-processing.html");
        this.template = '`' + fs.readFileSync(tplPreviewPath, "utf-8") + '`';
        this.templateProcessing = '`' + fs.readFileSync(tplPreviewProcessingPath, "utf-8") + '`';
        this.rendered = null;
        this.uiStatus = "";
        this.images = [];
        this.imageError = "";
        this.error = "";
    }
    provideTextDocumentContent(uri, token) {
        let image = this.images[0];
        let images = this.images.reduce((p, c) => {
            return `${p}<img src="${c}">`;
        }, "");
        let imageError;
        let error;
        try {
            switch (this.status) {
                case previewStatus.default:
                case previewStatus.error:
                    let zoomUpperLimit = this.zoomUpperLimit;
                    let status = this.uiStatus;
                    let nonce = Math.random().toString(36).substr(2);
                    let tmplPath = "file:///" + path.join(common_1.context.extensionPath, "templates");
                    let pageInfo = common_1.localize(20, null);
                    imageError = this.imageError;
                    error = this.error.replace(/\n/g, "<br />");
                    if (!image)
                        image = imageError;
                    return eval(this.template);
                case previewStatus.processing:
                    let icon = "file:///" + path.join(common_1.context.extensionPath, "images", "icon.png");
                    let processingTip = common_1.localize(9, null);
                    image = tools_1.calculateExportPath(this.rendered, config_1.config.previewFileType);
                    image = tools_1.addFileIndex(image, 0, this.rendered.pageCount);
                    if (!fs.existsSync(image))
                        image = "";
                    else
                        image = "file:///" + image;
                    return eval(this.templateProcessing);
                default:
                    return "";
            }
        }
        catch (error) {
            return error;
        }
    }
    setUIStatus(status) {
        this.uiStatus = status;
    }
    update(processingTip) {
        //FIXME: last update may not happen due to killingLock
        if (this.killingLock)
            return;
        if (this.task)
            this.task.canceled = true;
        if (this.task && this.task.processes && this.task.processes.length) {
            this.killingLock = true;
            //kill lats unfinished task.
            // let pid = this.process.pid;
            this.task.processes.map((p, i) => {
                p.kill();
                if (i == this.task.processes.length - 1) {
                    //start next preview only when last process is killed
                    p.on('exit', (code) => {
                        // console.log(`killed (${pid} ${code}) and restart!`);
                        this.task.processes = [];
                        this.doUpdate(processingTip);
                        this.killingLock = false;
                    });
                }
            });
            return;
        }
        this.doUpdate(processingTip);
    }
    get TargetChanged() {
        let current = new diagram_1.Diagram().GetCurrent();
        if (!current.content)
            return false;
        let changed = (!this.rendered || !this.rendered.isEqual(current));
        if (changed) {
            this.rendered = current;
            this.error = "";
            this.images = [];
            this.imageError = "";
            this.uiStatus = "";
        }
        return changed;
    }
    doUpdate(processingTip) {
        let diagram = new diagram_1.Diagram().GetCurrent();
        if (!diagram.content) {
            this.status = previewStatus.error;
            this.error = common_1.localize(3, null);
            this.images = [];
            this.Emittor.fire(this.Uri);
            return;
        }
        const previewFileType = config_1.config.previewFileType;
        const previewMimeType = previewFileType === 'png' ? 'png' : "svg+xml";
        this.zoomUpperLimit = previewMimeType === 'png';
        let task = exportToBuffer_1.exportToBuffer(diagram, previewFileType);
        this.task = task;
        // console.log(`start pid ${this.process.pid}!`);
        if (processingTip)
            this.processing();
        task.promise.then(result => {
            if (task.canceled)
                return;
            this.task = null;
            this.status = previewStatus.default;
            this.error = "";
            this.imageError = "";
            this.images = result.reduce((p, buf) => {
                let b64 = buf.toString('base64');
                if (!b64)
                    return p;
                p.push(`data:image/${previewMimeType};base64,${b64}`);
                return p;
            }, []);
            this.Emittor.fire(this.Uri);
        }, error => {
            if (task.canceled)
                return;
            this.task = null;
            this.status = previewStatus.error;
            let err = tools_1.parseError(error)[0];
            this.error = err.error;
            let b64 = err.out.toString('base64');
            if (!(b64 || err.error))
                return;
            this.imageError = `data:image/${previewMimeType};base64,${b64}`;
            this.Emittor.fire(this.Uri);
        });
    }
    //display processing tip
    processing() {
        this.status = previewStatus.processing;
        this.Emittor.fire(this.Uri);
    }
    register() {
        this.initialize();
        let disposable;
        let disposables = [];
        //register provider
        disposable = vscode.workspace.registerTextDocumentContentProvider('plantuml', this);
        disposables.push(disposable);
        //register command
        disposable = vscode.commands.registerCommand('plantuml.preview', () => {
            var editor = vscode.window.activeTextEditor;
            if (!editor)
                return;
            let ds = new diagram_1.Diagrams().AddDocument(editor.document);
            if (!ds.diagrams.length)
                return;
            //reset in case that starting commnad in none-diagram area, 
            //or it may show last error image and may cause wrong "TargetChanged" result on cursor move.
            this.reset();
            this.TargetChanged;
            return vscode.commands.executeCommand('vscode.previewHtml', this.Uri, vscode.ViewColumn.Two, common_1.localize(17, null))
                .then(success => {
                //active source editor
                vscode.window.showTextDocument(editor.document);
                //update preview
                if (config_1.config.previewAutoUpdate)
                    this.startWatch();
                else
                    this.stopWatch();
                this.update(true);
                return;
            }, reason => {
                vscode.window.showErrorMessage(reason);
            });
        });
        disposables.push(disposable);
        return disposables;
    }
    startWatch() {
        if (this.watchDisposables.length) {
            return;
        }
        let disposable;
        let disposables = [];
        //register watcher
        let lastTimestamp = new Date().getTime();
        disposable = vscode.workspace.onDidChangeTextDocument(e => {
            if (!e || !e.document || !e.document.uri)
                return;
            if (e.document.uri.scheme == "plantuml")
                return;
            lastTimestamp = new Date().getTime();
            setTimeout(() => {
                if (new Date().getTime() - lastTimestamp >= 400) {
                    if (!new diagram_1.Diagram().GetCurrent().content)
                        return;
                    this.update(false);
                }
            }, 500);
        });
        disposables.push(disposable);
        disposable = vscode.window.onDidChangeTextEditorSelection(e => {
            lastTimestamp = new Date().getTime();
            setTimeout(() => {
                if (new Date().getTime() - lastTimestamp >= 400) {
                    if (!this.TargetChanged)
                        return;
                    this.update(true);
                }
            }, 500);
        });
        disposables.push(disposable);
        //stop watcher when preview window is closed
        disposable = vscode.workspace.onDidCloseTextDocument(e => {
            if (e.uri.scheme === this.Uri.scheme) {
                this.stopWatch();
            }
        });
        disposables.push(disposable);
        this.watchDisposables = disposables;
    }
    stopWatch() {
        for (let d of this.watchDisposables) {
            d.dispose();
        }
        this.watchDisposables = [];
    }
}
exports.previewer = new Previewer();
//# sourceMappingURL=previewer.js.map