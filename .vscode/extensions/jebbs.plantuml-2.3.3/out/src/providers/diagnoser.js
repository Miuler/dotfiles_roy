"use strict";
const vscode = require('vscode');
const diagram_1 = require('../plantuml/diagram/diagram');
const common_1 = require('../plantuml/common');
class Diagnoser {
    constructor(ext) {
        this.langID = ext.packageJSON.contributes.languages[0].id;
        this.extName = ext.packageJSON.name;
    }
    register() {
        let ds = [];
        this.DiagnosticCollection = vscode.languages.createDiagnosticCollection(this.extName);
        ds.push(this.DiagnosticCollection, vscode.workspace.onDidOpenTextDocument(doc => this.diagnose(doc)), vscode.workspace.onDidChangeTextDocument(e => this.diagnose(e.document)), vscode.workspace.onDidCloseTextDocument(doc => this.removeDiagnose(doc)));
        return ds;
    }
    diagnose(document) {
        if (document.languageId !== this.langID)
            return;
        let diagrams = new diagram_1.Diagrams().AddDocument(document);
        let diagnostics = [];
        let names = {};
        diagrams.diagrams.map(d => {
            let range = document.lineAt(d.start.line).range;
            if (!d.titleRaw) {
                diagnostics.push(new vscode.Diagnostic(range, common_1.localize(30, null), vscode.DiagnosticSeverity.Warning));
            }
            if (names[d.title]) {
                diagnostics.push(new vscode.Diagnostic(range, common_1.localize(31, null, d.title), vscode.DiagnosticSeverity.Error));
            }
            else {
                names[d.title] = true;
            }
        });
        this.removeDiagnose(document);
        this.DiagnosticCollection.set(document.uri, diagnostics);
    }
    removeDiagnose(document) {
        this.DiagnosticCollection.delete(document.uri);
    }
}
exports.Diagnoser = Diagnoser;
//# sourceMappingURL=diagnoser.js.map