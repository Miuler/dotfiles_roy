'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const config_1 = require('./plantuml/config');
const previewer_1 = require('./providers/previewer');
const symboler_1 = require("./providers/symboler");
const formatter_1 = require("./providers/formatter");
const messages_1 = require("./plantuml/messages");
const common_1 = require("./plantuml/common");
const exportCurrent_1 = require('./commands/exportCurrent');
const exportDocument_1 = require('./commands/exportDocument');
const exportWorkspace_1 = require('./commands/exportWorkspace');
const urlCurrent_1 = require('./commands/urlCurrent');
const urlDocument_1 = require('./commands/urlDocument');
const previewStatus_1 = require('./commands/previewStatus');
const index_1 = require('./markdown-it-plantuml/index');
const diagnoser_1 = require('./providers/diagnoser');
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    common_1.setContext(context);
    try {
        const ext = vscode.extensions.getExtension("jebbs.plantuml");
        const version = ext.packageJSON.version;
        messages_1.notifyOnNewVersion(context, version);
        context.subscriptions.push(config_1.config.watch());
        //register commands
        context.subscriptions.push(new exportCurrent_1.CommandExportCurrent());
        context.subscriptions.push(new exportDocument_1.CommandExportDocument());
        context.subscriptions.push(new exportWorkspace_1.CommandExportWorkspace());
        context.subscriptions.push(new urlCurrent_1.CommandURLCurrent());
        context.subscriptions.push(new urlDocument_1.CommandURLDocument());
        context.subscriptions.push(new previewStatus_1.CommandPreviewStatus());
        //register preview provider
        context.subscriptions.push(...previewer_1.previewer.register());
        //register symbol provider
        context.subscriptions.push(...symboler_1.symboler.register());
        //register formatter provider
        context.subscriptions.push(...formatter_1.formatter.register());
        //register diagnoser
        context.subscriptions.push(...new diagnoser_1.Diagnoser(ext).register());
        return {
            extendMarkdownIt(md) {
                return md.use(index_1.plantumlPlugin(md));
            }
        };
    }
    catch (error) {
        common_1.outputPanel.clear();
        common_1.outputPanel.append(error);
    }
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() {
    previewer_1.previewer.stopWatch();
    common_1.outputPanel.dispose();
    common_1.bar.dispose();
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map