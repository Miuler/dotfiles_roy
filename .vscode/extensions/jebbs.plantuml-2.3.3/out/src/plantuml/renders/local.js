"use strict";
const fs = require('fs');
const child_process = require('child_process');
const path = require('path');
const config_1 = require('../config');
const common_1 = require('../common');
const tools_1 = require('../tools');
class LocalRender {
    constructor() {
        this.java = "java";
        this.javeInstalled = true;
        this.testJava();
    }
    testJava() {
        var process = child_process.exec(this.java + " -version", (e, stdout, stderr) => {
            if (e instanceof Error) {
                this.javeInstalled = false;
            }
        });
    }
    /**
     * Indicates the exporter should limt concurrency or not.
     * @returns boolean
     */
    limitConcurrency() {
        return true;
    }
    /**
     * formats return an string array of formats that the exporter supports.
     * @returns an array of supported formats
     */
    formats() {
        return [
            "png",
            "svg",
            "eps",
            "pdf",
            "vdx",
            "xmi",
            "scxml",
            "html",
            "txt",
            "utxt",
            "latex",
            "latex:nopreamble"
        ];
    }
    /**
     * export a diagram to file or to Buffer.
     * @param diagram The diagram to export.
     * @param format format of export file.
     * @param savePath if savePath is given, it exports to a file, or, to Buffer.
     * @returns ExportTask.
     */
    render(diagram, format, savePath) {
        if (!this.javeInstalled) {
            let pms = Promise.reject(common_1.localize(5, null));
            return { promise: pms };
        }
        if (!fs.existsSync(config_1.config.jar)) {
            let pms = Promise.reject(common_1.localize(6, null, common_1.context.extensionPath));
            return { promise: pms };
        }
        let processes = [];
        let buffers = [];
        //make a promise chain that export only one page at a time
        //but processes are all started at the begining, and recorded for later process.
        let pms = [...Array(diagram.pageCount).keys()].reduce((pChain, index) => {
            let params = [
                '-Djava.awt.headless=true',
                '-jar',
                config_1.config.jar,
                "-pipeimageindex",
                `${index}`,
                "-t" + format,
                '-pipe',
                '-charset',
                'utf-8',
            ];
            if (diagram.dir && path.isAbsolute(diagram.dir))
                params.unshift('-Duser.dir=' + diagram.dir);
            //add user args
            params.unshift(...config_1.config.commandArgs);
            let process = child_process.spawn(this.java, params);
            processes.push(process);
            return pChain.then(result => {
                if (process.killed) {
                    buffers = null;
                    return Promise.resolve(null);
                }
                if (diagram.content !== null) {
                    process.stdin.write(diagram.content);
                    process.stdin.end();
                }
                let pms = new Promise((resolve, reject) => {
                    let buffs = [];
                    let bufflen = 0;
                    let stderror = '';
                    let savePath2 = "";
                    if (savePath) {
                        savePath2 = tools_1.addFileIndex(savePath, index, diagram.pageCount);
                        let f = fs.createWriteStream(savePath2);
                        process.stdout.pipe(f);
                    }
                    else {
                        process.stdout.on('data', function (x) {
                            buffs.push(x);
                            bufflen += x.length;
                        });
                    }
                    process.stdout.on('close', () => {
                        let stdout = Buffer.concat(buffs, bufflen);
                        if (!stderror) {
                            if (!savePath) {
                                buffers.push(stdout);
                            }
                            else {
                                buffers.push(new Buffer(savePath2));
                            }
                            resolve(null);
                        }
                        else {
                            stderror = common_1.localize(10, null, diagram.title, stderror);
                            reject({ error: stderror, out: stdout });
                        }
                    });
                    process.stderr.on('data', function (x) {
                        stderror += x;
                    });
                });
                return pms;
            }, err => {
                return Promise.reject(err);
            });
        }, Promise.resolve(new Buffer("")));
        return {
            processes: processes,
            promise: new Promise((resolve, reject) => {
                pms.then(() => {
                    resolve(buffers);
                }, err => {
                    reject(err);
                });
            })
        };
    }
}
exports.localRender = new LocalRender();
//# sourceMappingURL=local.js.map