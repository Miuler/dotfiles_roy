"use strict";
const vscode = require('vscode');
const fs = require('fs');
const path = require('path');
const config_1 = require('./config');
function mkdirs(dirname, callback) {
    fs.exists(dirname, function (exists) {
        if (exists) {
            callback();
        }
        else {
            mkdirs(path.dirname(dirname), function () {
                fs.mkdir(dirname, callback);
            });
        }
    });
}
exports.mkdirs = mkdirs;
function mkdirsSync(dirname) {
    if (fs.existsSync(dirname)) {
        return true;
    }
    else {
        if (mkdirsSync(path.dirname(dirname))) {
            fs.mkdirSync(dirname);
            return true;
        }
    }
}
exports.mkdirsSync = mkdirsSync;
function isSubPath(from, to) {
    let rel = path.relative(to, from);
    return !(path.isAbsolute(rel) || rel.substr(0, 2) == "..");
}
exports.isSubPath = isSubPath;
function parseError(error) {
    let nb = new Buffer("");
    if (typeof (error) === "string") {
        return [{ error: error, out: nb }];
    }
    else if (error instanceof TypeError || error instanceof Error) {
        let err = error;
        return [{ error: err.stack, out: nb }];
    }
    else if (error instanceof Array) {
        let arr = error;
        if (!arr || !arr.length)
            return [];
        if (instanceOfExportError(arr[0]))
            return error;
    }
    else {
        return [error];
    }
    return null;
    function instanceOfExportError(object) {
        return 'error' in object;
    }
}
exports.parseError = parseError;
function showMessagePanel(panel, message) {
    panel.clear();
    let errs;
    if (typeof (message) === "string") {
        panel.appendLine(message);
    }
    else if (errs = parseError(message)) {
        for (let e of errs) {
            panel.appendLine(e.error);
        }
    }
    else {
        panel.appendLine(new Object(message).toString());
    }
    panel.show();
}
exports.showMessagePanel = showMessagePanel;
class StopWatch {
    start() {
        this.startTime = new Date();
    }
    stop() {
        this.endTime = new Date();
        return this.runTime();
    }
    runTime() {
        return this.endTime.getTime() - this.startTime.getTime();
    }
}
exports.StopWatch = StopWatch;
function calculateExportPath(diagram, format) {
    let outDirName = config_1.config.exportOutDirName;
    let subDir = config_1.config.exportSubFolder;
    let dir = "";
    let wkdir = vscode.workspace.rootPath;
    //if current document is in workspace, organize exports in 'out' directory.
    //if not, export beside the document.
    if (wkdir && isSubPath(diagram.path, wkdir))
        dir = path.join(wkdir, outDirName);
    let exportDir = diagram.dir;
    if (!path.isAbsolute(exportDir))
        return "";
    if (dir && wkdir) {
        let temp = path.relative(wkdir, exportDir);
        exportDir = path.join(dir, temp);
    }
    if (subDir) {
        exportDir = path.join(exportDir, diagram.fileName);
    }
    return path.join(exportDir, diagram.title + "." + format);
}
exports.calculateExportPath = calculateExportPath;
function addFileIndex(fileName, index, count) {
    if (count == 1)
        return fileName;
    let bsName = path.basename(fileName);
    let ext = path.extname(fileName);
    return path.join(path.dirname(fileName), bsName.substr(0, bsName.length - ext.length) + "-" + (index + 1) + ext);
}
exports.addFileIndex = addFileIndex;
//# sourceMappingURL=tools.js.map