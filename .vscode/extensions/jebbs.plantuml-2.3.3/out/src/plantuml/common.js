"use strict";
const vscode = require('vscode');
const nls = require("vscode-nls");
const path_1 = require("path");
exports.outputPanel = vscode.window.createOutputChannel("PlantUML");
exports.bar = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left);
function setContext(ctx) {
    exports.context = ctx;
    nls.config({ locale: vscode.env.language });
    exports.localize = nls.loadMessageBundle(path_1.join(exports.context.extensionPath, "langs", "lang.json"));
}
exports.setContext = setContext;
//# sourceMappingURL=common.js.map