"use strict";
const vscode = require('vscode');
const fs = require('fs');
const path = require('path');
const common_1 = require('./common');
exports.RenderType = {
    Local: 'Local',
    PlantUMLServer: 'PlantUMLServer'
};
let conf = vscode.workspace.getConfiguration('plantuml');
class ConfigReader {
    _read(key) {
        return conf.get(key);
    }
    watch() {
        return vscode.workspace.onDidChangeConfiguration(() => {
            conf = vscode.workspace.getConfiguration('plantuml');
            this._jar = "";
        });
    }
    get jar() {
        return this._jar || (() => {
            let jar = this._read('jar');
            let intJar = path.join(common_1.context.extensionPath, "plantuml.jar");
            if (!jar) {
                jar = intJar;
            }
            else {
                if (!fs.existsSync(jar)) {
                    vscode.window.showWarningMessage(common_1.localize(19, null));
                    jar = intJar;
                }
            }
            this._jar = jar;
            return jar;
        })();
    }
    get fileExtensions() {
        let extReaded = this._read('fileExtensions').replace(/\s/g, "");
        let exts = extReaded || ".*";
        if (exts.indexOf(",") > 0)
            exts = `{${exts}}`;
        //REG: .* | .wsd | {.wsd,.java}
        if (!exts.match(/^(.\*|\.\w+|\{\.\w+(,\.\w+)*\})$/)) {
            throw new Error(common_1.localize(18, null, extReaded));
        }
        return exts;
    }
    get exportOutDirName() {
        return this._read('exportOutDirName') || "out";
    }
    get exportFormat() {
        return this._read('exportFormat');
    }
    get exportSubFolder() {
        return this._read('exportSubFolder');
    }
    get exportConcurrency() {
        return this._read('exportConcurrency') || 3;
    }
    get previewAutoUpdate() {
        return this._read('previewAutoUpdate');
    }
    get previewFileType() {
        return this._read('previewFileType') || "png";
    }
    get server() {
        return this._read('server') || "http://www.plantuml.com/plantuml";
    }
    get serverIndexParameter() {
        return this._read('urlServerIndexParameter');
    }
    get urlFormat() {
        return this._read('urlFormat');
    }
    get urlResult() {
        return this._read('urlResult') || "MarkDown";
    }
    get render() {
        return this._read('render');
    }
    get includes() {
        return this._read('includes') || [];
    }
    get commandArgs() {
        return this._read('commandArgs') || [];
    }
}
exports.config = new ConfigReader();
//# sourceMappingURL=config.js.map