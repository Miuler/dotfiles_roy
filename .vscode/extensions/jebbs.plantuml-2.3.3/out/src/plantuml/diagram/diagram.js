"use strict";
const vscode = require('vscode');
const path = require('path');
const title = require('./title');
const includer_1 = require('./includer');
exports.diagramStartReg = /@start/i;
exports.diagramEndReg = /@end/i;
class Diagrams {
    constructor() {
        this.diagrams = [];
    }
    Add(diagram) {
        this.diagrams.push(diagram);
        return this;
    }
    AddCurrent() {
        let d = new Diagram();
        d.GetCurrent();
        this.diagrams.push(d);
        return this;
    }
    AddDocument(document) {
        if (!document) {
            let editor = vscode.window.activeTextEditor;
            document = editor.document;
        }
        for (let i = 0; i < document.lineCount; i++) {
            let line = document.lineAt(i);
            if (exports.diagramStartReg.test(line.text)) {
                let d = new Diagram().DiagramAt(i, document);
                this.diagrams.push(d);
            }
        }
        return this;
    }
}
exports.Diagrams = Diagrams;
class Diagram {
    constructor(content) {
        this.pageCount = 1;
        this.index = 0;
        if (!content)
            return;
        this.content = content;
        this.lines = content.split('\n');
        this.getTitle();
        this.getPageCount();
    }
    GetCurrent() {
        let editor = vscode.window.activeTextEditor;
        if (editor)
            this.DiagramAt(editor.selection.anchor.line);
        return this;
    }
    DiagramAt(lineNumber, document) {
        if (!document)
            document = vscode.window.activeTextEditor.document;
        this.document = document;
        this.path = document.uri.fsPath;
        this.fileName = path.basename(this.path);
        let i = this.fileName.lastIndexOf(".");
        if (i >= 0)
            this.fileName = this.fileName.substr(0, i);
        this.dir = path.dirname(this.path);
        if (!path.isAbsolute(this.dir)) {
            if (vscode.workspace.rootPath) {
                this.dir = vscode.workspace.rootPath;
            }
        }
        for (let i = lineNumber; i >= 0; i--) {
            let line = document.lineAt(i);
            if (exports.diagramStartReg.test(line.text)) {
                this.start = line.range.start;
                break;
            }
            else if (i != lineNumber && exports.diagramEndReg.test(line.text)) {
                return this;
            }
        }
        for (let i = lineNumber; i < document.lineCount; i++) {
            let line = document.lineAt(i);
            if (exports.diagramEndReg.test(line.text)) {
                this.end = line.range.end;
                break;
            }
            else if (i != lineNumber && exports.diagramStartReg.test(line.text)) {
                return this;
            }
        }
        if (this.start && this.end) {
            this.lines = [];
            this.content = includer_1.includer.addIncludes(document.getText(new vscode.Range(this.start, this.end)));
            for (let i = this.start.line; i <= this.end.line; i++) {
                this.lines.push(document.lineAt(i).text);
            }
            this.getIndex();
            this.getTitle();
            this.getPageCount();
        }
        return this;
    }
    isEqual(d) {
        if (this.dir !== d.dir)
            return false;
        if (this.fileName !== d.fileName)
            return false;
        if (!this.start || !d.start)
            return false;
        if (!this.start.isEqual(d.start))
            return false;
        return true;
    }
    getPageCount() {
        let regNewPage = /^\s*newpage\b/i;
        for (let text of this.lines) {
            if (regNewPage.test(text))
                this.pageCount++;
        }
    }
    getTitle() {
        let RegFName = /@start(\w+)\s+(.+?)\s*$/i;
        let matches;
        ;
        if (matches = this.lines[0].match(RegFName)) {
            this.titleRaw = matches[2];
            this.title = title.Deal(this.titleRaw);
            return;
        }
        let inlineTitle = /^\s*title\s+(.+?)\s*$/i;
        let multLineTitle = /^\s*title\s*$/i;
        for (let text of this.lines) {
            if (inlineTitle.test(text)) {
                let matches = text.match(inlineTitle);
                this.titleRaw = matches[1];
            }
        }
        if (this.titleRaw) {
            this.title = title.Deal(this.titleRaw);
        }
        else if (this.start && this.end) {
            // this.title = `${this.fileName}@${this.start.line + 1}-${this.end.line + 1}`;
            if (this.index)
                this.title = `${this.fileName}-${this.index}`;
            else
                this.title = this.fileName;
        }
        else {
            this.title = "Untitled";
        }
    }
    getIndex() {
        if (!this.document)
            return;
        for (let i = 0; i < this.start.line; i++) {
            if (exports.diagramStartReg.test(this.document.lineAt(i).text))
                this.index++;
        }
    }
}
exports.Diagram = Diagram;
//# sourceMappingURL=diagram.js.map