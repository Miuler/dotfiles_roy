"use strict";
const common_1 = require('../common');
const appliedRender_1 = require('./appliedRender');
/**
 * export a diagram to file or to Buffer.
 * @param diagram The diagram to export.
 * @param format format of export file.
 * @param savePath if savePath is given, it exports to a file, or, to Buffer.
 * @param bar display prcessing message in bar if it's given.
 * @returns ExportTask.
 */
function exportDiagram(diagram, format, savePath, bar) {
    if (bar) {
        bar.show();
        bar.text = common_1.localize(7, null, diagram.title + "." + format.split(":")[0]);
    }
    return appliedRender_1.appliedRender().render(diagram, format, savePath);
}
exports.exportDiagram = exportDiagram;
//# sourceMappingURL=exportDiagram.js.map