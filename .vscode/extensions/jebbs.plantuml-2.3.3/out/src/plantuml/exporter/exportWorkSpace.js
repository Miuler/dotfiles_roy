"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const vscode = require('vscode');
const path = require('path');
const fs = require('fs');
const appliedRender_1 = require('./appliedRender');
const config_1 = require('../config');
const common_1 = require('../common');
const tools_1 = require('../tools');
const exportURIs_1 = require('./exportURIs');
function exportWorkSpace(para) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            if (!vscode.workspace.rootPath) {
                return;
            }
            let format = config_1.config.exportFormat;
            if (!format) {
                format = yield vscode.window.showQuickPick(appliedRender_1.appliedRender().formats());
                if (!format)
                    return;
            }
            common_1.outputPanel.clear();
            let exts = config_1.config.fileExtensions;
            if (!para) {
                doBuild(yield vscode.workspace.findFiles(`**/*${exts}`, ""), format);
            }
            else if (para instanceof vscode.Uri) {
                //commnad from the explorer/context
                if (fs.statSync(para.fsPath).isDirectory()) {
                    let relPath = path.relative(vscode.workspace.rootPath, para.fsPath);
                    doBuild(yield vscode.workspace.findFiles(`${relPath}/**/*${exts}`, ""), format);
                }
                else {
                    doBuild([para], format);
                }
            }
            else if (para instanceof Array) {
                //FIXME: directory uri(s) in array
                let uris = [];
                for (let p of para) {
                    if (p instanceof vscode.Uri) {
                        uris.push(p);
                    }
                }
                doBuild(uris, format);
            }
        }
        catch (error) {
            tools_1.showMessagePanel(common_1.outputPanel, error);
        }
    });
}
exports.exportWorkSpace = exportWorkSpace;
function doBuild(uris, format) {
    if (!uris.length) {
        vscode.window.showInformationMessage(common_1.localize(8, null));
        return;
    }
    let stopWatch = new tools_1.StopWatch();
    stopWatch.start();
    exportURIs_1.exportURIs(uris, format, common_1.bar).then((r) => __awaiter(this, void 0, void 0, function* () {
        stopWatch.stop();
        r = r;
        let results = r.results;
        let errors = r.errors;
        common_1.bar.hide();
        //uris.length: found documents count 
        //results.length: exported documents count 
        let viewReport = common_1.localize(26, null);
        let msg = "";
        let btn = "";
        if (!results.length) {
            msg = common_1.localize(29, null);
            if (!errors.length) {
                vscode.window.showInformationMessage(msg);
            }
            else {
                btn = yield vscode.window.showInformationMessage(msg, viewReport);
                if (btn === viewReport)
                    showReport();
            }
            return;
        }
        msg = common_1.localize(errors.length ? 12 : 13, null, results.length);
        btn = yield vscode.window.showInformationMessage(msg, viewReport);
        if (btn === viewReport)
            showReport();
        function showReport() {
            let fileCnt = 0;
            let diagramCnt = 0;
            let fileLst = results.reduce((list, diagrams) => {
                if (!diagrams || !diagrams.length)
                    return list;
                diagramCnt += diagrams.length;
                return list + diagrams.reduce((oneDiagramList, files) => {
                    if (!files || !files.length)
                        return oneDiagramList;
                    fileCnt += files.length;
                    return oneDiagramList + "\n" + files.join("\n");
                }, "");
            }, "");
            let report = common_1.localize(28, null, results.length, diagramCnt, fileCnt, stopWatch.runTime() / 1000) + fileLst;
            if (errors.length) {
                report += "\n" + errors.reduce((p, c) => {
                    return p + (p ? "\n" : "") + c.error;
                }, "");
            }
            tools_1.showMessagePanel(common_1.outputPanel, report);
        }
    }));
}
//# sourceMappingURL=exportWorkSpace.js.map