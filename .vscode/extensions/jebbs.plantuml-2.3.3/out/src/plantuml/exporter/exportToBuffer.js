"use strict";
const exportDiagram_1 = require('./exportDiagram');
/**
 * export diagram to buffer
 * @param diagram the diagram to export.
 * @param format format of export file.
 * @param bar display prcessing message in bar if it's given.
 * @returns ExportTask.
 */
function exportToBuffer(diagram, format, bar) {
    return exportDiagram_1.exportDiagram(diagram, format, "", bar);
}
exports.exportToBuffer = exportToBuffer;
//# sourceMappingURL=exportToBuffer.js.map