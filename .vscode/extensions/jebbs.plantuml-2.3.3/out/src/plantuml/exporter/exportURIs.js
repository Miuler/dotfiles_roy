"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const vscode = require('vscode');
const config_1 = require('../config');
const common_1 = require('../common');
const tools_1 = require('../tools');
const appliedRender_1 = require('./appliedRender');
const exportURI_1 = require('./exportURI');
/**
 * export diagrams of multiple vscode.Uris to file
 * @param uris the uris to export.
 * @param format format of export file.
 * @param bar display prcessing message in bar if it's given.
 * @returns Promise<Buffer[][]>. A promise of exportURIsResult
 */
function exportURIs(uris, format, bar) {
    return __awaiter(this, void 0, void 0, function* () {
        if (appliedRender_1.appliedRender().limitConcurrency()) {
            let concurrency = config_1.config.exportConcurrency;
            return exportURIsLimited(uris, format, concurrency, bar);
        }
        else {
            return exportURIsUnLimited(uris, format, bar);
        }
    });
}
exports.exportURIs = exportURIs;
function exportURIsLimited(uris, format, concurrency, bar) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!uris.length) {
            vscode.window.showInformationMessage(common_1.localize(8, null));
            return;
        }
        let errors = [];
        let results = [];
        let promiseChain = uris.reduce((prev, uri, index) => {
            return prev.then(result => {
                if (result && result.length)
                    results.push(result);
                return exportURI_1.exportURI(uri, format, bar);
            }, errs => {
                errors.push(...tools_1.parseError(common_1.localize(11, null, errs.length, uris[index - 1].fsPath)));
                errors.push(...tools_1.parseError(errs));
                // continue next file
                return exportURI_1.exportURI(uri, format, bar);
            });
        }, Promise.resolve([]));
        return new Promise((resolve, reject) => {
            promiseChain.then(result => {
                if (result && result.length)
                    results.push(result);
                resolve({ results: results, errors: errors });
            }, errs => {
                errors.push(...tools_1.parseError(common_1.localize(11, null, errs.length, uris[uris.length - 1].fsPath)));
                errors.push(...tools_1.parseError(errs));
                resolve({ results: results, errors: errors });
            });
        });
    });
}
function exportURIsUnLimited(uris, format, bar) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!uris.length) {
            vscode.window.showInformationMessage(common_1.localize(8, null));
            return;
        }
        let errors = [];
        let results = [];
        let promises = uris.map((uri, index) => exportURI_1.exportURI(uri, format, bar).then(result => {
            if (result && result.length)
                results.push(result);
        }, errs => {
            errors.push(...tools_1.parseError(common_1.localize(11, null, errs.length, uris[index].fsPath)));
            errors.push(...tools_1.parseError(errs));
        }));
        return new Promise((resolve, reject) => {
            Promise.all(promises).then(() => resolve({ results: results, errors: errors }), () => resolve({ results: results, errors: errors }));
        });
    });
}
//# sourceMappingURL=exportURIs.js.map