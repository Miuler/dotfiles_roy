"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const vscode = require('vscode');
const path = require('path');
const appliedRender_1 = require('./appliedRender');
const diagram_1 = require('../diagram/diagram');
const config_1 = require('../config');
const common_1 = require('../common');
const tools_1 = require('../tools');
const exportDiagrams_1 = require('./exportDiagrams');
function exportDocument(all) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let stopWatch = new tools_1.StopWatch();
            stopWatch.start();
            let editor = vscode.window.activeTextEditor;
            if (!editor) {
                vscode.window.showInformationMessage(common_1.localize(0, null));
                return;
            }
            if (!path.isAbsolute(editor.document.fileName)) {
                vscode.window.showInformationMessage(common_1.localize(1, null));
                return;
            }
            ;
            let format = config_1.config.exportFormat;
            if (!format) {
                format = yield vscode.window.showQuickPick(appliedRender_1.appliedRender().formats());
                if (!format)
                    return;
            }
            common_1.outputPanel.clear();
            let ds = new diagram_1.Diagrams();
            if (all) {
                ds.AddDocument();
                if (!ds.diagrams.length) {
                    vscode.window.showInformationMessage(common_1.localize(2, null));
                    return;
                }
            }
            else {
                let dg = new diagram_1.Diagram().GetCurrent();
                if (!dg.content) {
                    vscode.window.showInformationMessage(common_1.localize(3, null));
                    return;
                }
                ds.Add(dg);
                editor.selections = [new vscode.Selection(dg.start, dg.end)];
            }
            exportDiagrams_1.exportDiagrams(ds.diagrams, format, common_1.bar).then((results) => __awaiter(this, void 0, void 0, function* () {
                stopWatch.stop();
                common_1.bar.hide();
                if (!results.length)
                    return;
                let viewReport = common_1.localize(26, null);
                let btn = yield vscode.window.showInformationMessage(common_1.localize(4, null), viewReport);
                if (btn !== viewReport)
                    return;
                let fileCnt = 0;
                let fileLst = results.reduce((p, c) => {
                    fileCnt += c.length;
                    return p + "\n" + c.join("\n");
                }, "");
                tools_1.showMessagePanel(common_1.outputPanel, common_1.localize(27, null, ds.diagrams.length, fileCnt, stopWatch.runTime() / 1000) + fileLst);
            }), error => {
                common_1.bar.hide();
                tools_1.showMessagePanel(common_1.outputPanel, error);
            });
        }
        catch (error) {
            tools_1.showMessagePanel(common_1.outputPanel, error);
        }
        return;
    });
}
exports.exportDocument = exportDocument;
//# sourceMappingURL=exportDocument.js.map