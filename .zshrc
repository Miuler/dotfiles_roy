[[ $- != *i* ]] && return

setxkbmap -option 'ctrl:nocaps'

eval $(dircolors -b ~/.dircolors)
xrdb -merge ~/.Xresources

for file in $HOME/.{shell_exports,shell_aliases,shell_user,shell_functions,shell_config}; do
  [ -r "$file" ] && [ -f "$file" ] && . "$file";
done;
unset file;

bindkey "\e[7~" beginning-of-line
bindkey "\e[8~" end-of-line
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

# sudo usermod -a -G docker,lxd rsurj
# sudo usermod -G docker,lxd rsurj
# newgrp docker
# newgrp lxd

. "$HOME/.zprezto/init.zsh"
