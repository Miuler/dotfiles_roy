[[ $- != *i* ]] && return

for file in $HOME/.{shell_exports,shell_aliases,shell_user,shell_functions,shell_config}; do
  [ -r "$file" ] && [ -f "$file" ] && . "$file";
done;
unset file;

source_bash_completion() {
  local f
  [[ $BASH_COMPLETION ]] && return 0
  for f in /{etc,usr/share/bash-completion}/bash_completion; do
    if [[ -r $f ]]; then
      . "$f"
      return 0;
    fi
  done
}
source_bash_completion
unset -f source_bash_completion

bind '"\e[7~" beginning-of-line'
bind '"\e[8~" end-of-line'
bind ' "^[[1;5C" forward-word'
bind '"^[[1;5D" backward-word'
bind '"^[[3~" delete-char'
bind '"^[3;5~" delete-char'

#sudo usermod -G docker,lxd rsurj
#newgrp docker
#newgrp lxd

[[ -s "$HOME/lib/azure-cli/az.completion" ]] && . "$HOME/lib/azure-cli/az.completion"
[[ -s "$NVM_DIR/bash_completion" ]] && . "$NVM_DIR/bash_completion"
