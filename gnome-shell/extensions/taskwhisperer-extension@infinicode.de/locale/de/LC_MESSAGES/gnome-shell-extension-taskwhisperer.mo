��    $      <  5   \      0     1  �   B  �   �  /   �                $     1     8     ?     F     V     c     h     z     �     �     �     �     �     �     �  Y   �     #  	   5     ?     H     N  *   \  
   �     �  	   �     �     �  	   �  x  �     6  �   M  �   
	  9   �	      
     2
     7
  	   @
     J
  	   Q
     [
     s
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  Y        b     k     w     �     �  /   �     �     �     �     �     �  	   �                                  #                                             	       
                                                  "      !   $                                   %d Task %d Tasks <span size="small">This program comes with ABSOLUTELY NO WARRANTY.
See the <a href="https://www.gnu.org/licenses/gpl">GNU General Public License, version 3 or later</a> for details.</span> <span>Taskwhisperer is a extension for TaskWarrior Application <a href="https://taskwarrior.org">https://taskwarrior.org</a>. It is to display upcoming tasks and task details as well as to create and modify them.</span> A description is required to create a new task! Additional Arguments All Annotations: Cancel Center Create Create New Task Description: Due: Enter Description Enter DueDate Identifier: Layout Left Maintained by Modify Modify Task Modify: Please make sure taskd is proper configured, otherwise you will get errors fetching data! Position in Panel Priority: Project: Right Set Task Done Sorry, that didn't work. Please try again. Start task Status: Stop task Tags: Urgency: Version:  Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-14 00:53+0200
PO-Revision-Date: 2016-04-16 23:40+0200
Last-Translator: Florijan Hamzic <florijanh@gmail.com>
Language-Team: German
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 %d Aufgabe %d Aufgaben <span size="small">This program comes with ABSOLUTELY NO WARRANTY.
See the <a href="https://www.gnu.org/licenses/gpl">GNU General Public License, version 3 or later</a> for details.</span> <span>Taskwhisperer is a extension for TaskWarrior Application <a href="https://taskwarrior.org">https://taskwarrior.org</a>. It is to display upcoming tasks and task details as well as to create and modify them.</span> Eine Beschreibung ist notwendig um ein Task zu erstellen! Weitere Parameter Alle Notizen: Abbrechen Mittig Erstellen Neuen Eintrag erstellen Beschreibung: Fälligkeit: Beschreibung eingeben Fälligkeit eingeben Identifikator: Layout Links Maintained von Ändern Task bearbeiten Bearbeiten: Please make sure taskd is proper configured, otherwise you will get errors fetching data! Position Priorität: Projekt: Rechts Task erledigt Ein Problem trat auf. Probieren Sie es nochmal. Starten Status: Stoppen Tags: Dringlichkeit: Version:  