#!/bin/bash

export USER_DOTFILE=rsurj
export DOTFILES_DIR="/$USER_DOTFILE/dotfiles"
cd $DOTFILES_DIR

answer_is_yes() {
  [[ "$REPLY" =~ ^[Yy]$ ]] \
    && return 0 \
    || return 1
}
ask_for_confirmation() {
  print_question "$1 (y/n) "
  read -n 1
  printf "\n"
}
execute() {
  $1 &> /dev/null
  print_result $? "${2:-$1}"
}
mkd() {
  if [ -n "$1" ]; then
    if [ -e "$1" ]; then
      if [ ! -d "$1" ]; then
        print_error "$1 - a file with the same name already exists!"
      else
        print_success "$1"
      fi
    else
      execute "mkdir -p $1" "$1"
    fi
  fi
}
print_error() {
  # Print output in red
  printf "\e[0;31m  [✖] $1 $2\e[0m\n"
}
print_info() {
  # Print output in purple
  printf "\n\e[0;35m $1\e[0m\n\n"
}
print_question() {
  # Print output in yellow
  printf "\e[0;33m  [?] $1\e[0m"
}
print_result() {
  [ $1 -eq 0 ] \
    && print_success "$2" \
    || print_error "$2"

  [ "$3" == "true" ] && [ $1 -ne 0 ] \
    && exit
}
print_success() {
  # Print output in green
  printf "\e[0;32m  [✔] $1\e[0m\n"
}
post_install() {
  
  print_info "adding chrome flags..."
  echo "
  --disk-cache-dir=/tmp/cache
  --disk-cache-size=104857600
  --enable-low-end-device-mode
  --enable-low-res-tiling
  --process-per-site
  #--incognito
  " > ~/.config/chromium-flags.conf
  
  print_info "configuring vagrant... "
  vagrant plugin install vagrant-vbguest vagrant-proxyconf
  
  print_info "configuring mariadb... "
  # systemctl enable mariadb
  # systemctl start mariadb
  # sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
  # #mysql -uroot -p
  # systemctl stop mariadb
  # systemctl disable mariadb
  #enabling php/mariadb connection
  echo "
  extension=pdo_mysql.so
  extension=mysqli.so
  " | sudo tee -a /etc/php/php.ini

  
  print_info "configuring laravel... "
  composer global require "laravel/installer"
  echo "
  export PATH=\"\$HOME/.config/composer/vendor/bin:\$PATH\"" >> $HOME/.bashrc
  
  print_info "configuring postgresql... "
  # systemctl enable postgresql
  # systemctl start postgresql
  # sudo -u postgres -i
  # initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data'
  # psql -c "CREATE USER rsurj WITH PASSWORD 'rsurj';"
  # psql -c "ALTER USER rsurj WITH SUPERUSER;"
  # createdb rsurj
  # exit
  # systemctl stop postgresql
  # systemctl disable postgresql
  
  print_info "configuring docker..."
  sudo tee /etc/modules-load.d/loop.conf <<< "loop"
  sudo modprobe loop
  sudo usermod -G docker rsurj
  newgrp docker
  print_info "enabling sshd service... "
  systemctl enable sshd
  systemctl start sshd
  
  print_info "Configuring git... "
  git config --global credential.helper 'cache --timeout 36000'

  print_info "configuring NVM..."
  #nvm install 6.11.3

  print_info "Configuring vscode..."
  ln -fs $DOTFILES_DIR/.vscode/settings.json ~/.config/Code/User/settings.json
  ln -fs $DOTFILES_DIR/.vscode/snippets ~/.config/Code/User
  ln -fs $DOTFILES_DIR/.vscode.rsurj/settings.json /rsurj/.vscode/settings.json

  print_info "configuring npm..."
  npm config set save-exact = true
  #npm_packages=(
    #diff-so-fancy
    #git-recent
    #git-open
    #gulp
    #http-server
    #servedir
    #flow-bin
    #flow-typed
    #npm-check-updates
    #webpack
    #nodemon
    #svgo
    #is-up -g
    #pageres-cli
    #caniuse-cmd
    #npm-name-cli
    #ember-cli
    #eslint
    #eslint-plugin-react
    #bower
    #yo
    #gulp
    #@angular/cli
  # )
  #sudo npm install -g "${npm_packages[@]}"

  print_info "configuring gradle... "
  mkdir -p ~/.gradle/init.d
  echo "initscript {
    repositories {
        maven { url 'https://plugins.gradle.org/m2' }
    }

    dependencies {
        classpath 'com.gradle:build-scan-plugin:1.6'
    }
}
rootProject {
    apply plugin: com.gradle.scan.plugin.BuildScanPlugin

    buildScan {
        licenseAgreementUrl = 'https://gradle.com/terms-of-service'
        licenseAgree = 'yes'
    }
}" > ~/.gradle/init.d/buildScan.gradle
  echo "org.gradle.daemon=true
org.gradle.parallel=true
org.gradle.configureondemand=true
" > ~/.gradle/gradle.properties
  
  print_info "configuring aws-cli... "
  echo "
  export PATH=\"\$HOME/.local/bin:\$PATH\"" >> $HOME/.bashrc

  print_info "configuring mongodb... "

  print_info "configuring terminal environment..."
  
  print_info "configuring weechat... "

  print_info "Configuring SQL SERVER"
  sudo systemctl start mssql-server
  sudo /opt/mssql/bin/mssql-conf setup accept-eula

  print_info "Configuring Spotify"
  mkdir -p ~/.config/spotify
  touch ~/.config/spotify/prefs

  print_info "configuring npm"
  touch ~/.npmrc

  print_info "configuring curl/wget"
  touch ~/.curlrc

  print_info "Configuring ZSH"
  # chsh -s /usr/bin/zsh

  print_info "Configuring GNOME Shell"
  sudo rm -rf $HOME/.local/share/gnome-shell
  ln -fs $DOTFILES_DIR/gnome-shell ~/.local/share

  print_info "Configuring urxvtd"
  sudo rm -rf /etc/systemd/urxvtd@service
  echo "[Unit]
Description=RXVT-Unicode Daemon

[Service]
User=%i
ExecStart=/usr/bin/urxvtd -q -o

[Install]
WantedBy=multi-user.target" | sudo tee -a /etc/systemd/system/urxvtd@.service
sudo systemctl enable urxvtd@rsurj.service
sudo systemctl start urxvtd@rsurj.service

  print_info "Configuring tmux"
  echo"[Unit]
Description=Start tmux in detached session

[Service]
Type=forking
User=%I
ExecStart=/usr/bin/tmux new-session -s %u -d
ExecStop=/usr/bin/tmux kill-session -t %u

[Install]
WantedBy=multi-user.target" | sudo tee -a /etc/systemd/system/tmux@.service
sudo systemctl enable tmux@rsurj.service
sudo systemctl start tmux@rsurj.service


  print_info "Configuring virtualbox"
  sudo modprobe vboxdrv

  print_info "Configuring LXD"
  sudo systemctl enable lxd
  sudo systemctl start lxd
  sudo usermod -G lxd rsurj
  newgrp lxd
  lxc profile set default security.privileged true
  lxc profile set default security.nesting true
cat <<EOF | sudo lxd init
yes
default
dir
yes
all
8443
@G4t0_.201e
@G4t0_.201e
yes
yes
lxdbr0
auto
auto  
EOF

  print_info "cleaning all the mess... "
  sudo pacman -Sc
  yaourt -Qtd 

  # Generating GPG key
#RSA & RSA
#4096
#0 
gpg --full-generate-key
unset gpgkey

}

while true; do
  read -p "Warning: this will overwrite your current dotfiles. Continue? [y/n] " yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done
echo -n "symlinking files... "
ignored_files=(
  .
  ..
  bootstrap.sh
  .vscode.rsurj
  gnome-shell
  .config
  LICENCE
  README.md
  remote-setup.sh
  .git
  startup.desktop
  ansible
)  
local i=''
local sourceFile=''
local targetFile=''
for filename in .* *; do 
  if [[ " ${ignored_files[*]} " == *" $filename "* ]]; then
    print_info "no symlink for → $filename";
  else
    sourceFile="$DOTFILES_DIR/$filename"
    targetFile="$HOME/$filename"
    # if directory or file
    if [ ! -e "$targetFile" ]; then
      execute "ln -fs $sourceFile $HOME" "$targetFile → $sourceFile"
    elif [ "$(readlink "$targetFile")" == "$sourceFile" ]; then
      print_success "$targetFile → $sourceFile"
    else
      ask_for_confirmation "'$targetFile' already exists, do you want to overwrite it?"
      if answer_is_yes; then
        rm -rf "$targetFile"
        execute "ln -fs $sourceFile $targetFile" "$targetFile → $sourceFile"
      else
        print_error "$targetFile → $sourceFile"
      fi
    fi
  fi
done
# binaries
for i in $HOME/bin/*; do 
  echo "Changing access permissions for binary script :: ${i##*/}"
  chmod +rwx $HOME/bin/${i##*/}
done
print_info "done"

# Ansible
#cd $DOTFILES_DIR/ansible
#ansible-playbook -i hosts playbook.yml
#echo -n "reloading ~/.zshrc & ~/.bashrc... "
#. ~/.zshrc
#. ~/.bashrc
#post_install



# sudo pip install hangups
# yaourt -S aur/corebird

# yaourt -S aur/discord
#gpg --recv-keys B6C8F98282B944E3B0D5C2530FC3042E345AD05D


#atom apm


# pacman -S atom
# pacman -S apm
# apm install atom-ide-ui
